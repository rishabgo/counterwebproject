package com.qaagility.controller;

public class Counter {

    public int calculate(int aval, int bval) {
        if (bval == 0){
            return Integer.MAX_VALUE;
       }
        else{
            return aval / bval;
      }
    }

}
