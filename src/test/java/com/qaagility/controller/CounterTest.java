package com.qaagility.controller;

import static org.junit.Assert.*;
import org.junit.Test;

public class CounterTest{

@Test
public void testcalculate(){

int result=  new Counter().calculate(1,0);
assertEquals("max",Integer.MAX_VALUE,result);
}

@Test
public void testcalculate2(){

 int result = new Counter().calculate(2,1);
assertEquals("maxval",2,result);
}
}
